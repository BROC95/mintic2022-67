package sentencias.control.flujo;


public class Sentencias {
    
    public static void main(String[] args) {
        
         int edad = 18;
                         
        if(edad >= 18){
            System.out.println("Es adulto");
        }else if(edad > 12 && edad < 15){
           System.out.println("Es adolescente junior"); 
        }else if(edad >= 15 && edad < 18){
           System.out.println("Es adolescente "); 
        }else{
            System.out.println("Es un niño"); 
        }
        
        
        /// while y do while
        
        int cont = 10;
         
        while (cont < 18){
             System.out.println("cont = " + cont++);  
        }
                
        do {
            System.out.println("cont = " + cont++); 
        } while (cont < 18);
        
        
        /// for utilizando la variable cont       
        int cont1 = 10;                
             
        for (int i = 0; cont1 < 18; i++) {
            System.out.println("cont = " + cont1++); 
        }
        
        /// utilizando la variable i
        for (int i = 10; i < 18; i++) {
            System.out.println("i = " + i); 
        }
        
        // switch     
        // recibe los typos de datos ( byte, short, char, int String )
        char letra = 'A';
        
        String result = "la letra es: ";
        
        switch(letra){
            case 'A':
                result += letra;                        
                break;
            case 'B':
                result += letra;                         
                break;
            default:
                 result += " no hubo coincidencia "; 
                break;
        }
        
        System.out.println("result --- " + result);
        
        
        //---------- otro ejercicio
        
          int num = 11;
        
        String resultado = "El número ";
        
        if(num > 0 && num < 11){
            switch(num){
            case 2:
            case 4:
            case 6:
            case 8:
                resultado +=  num + " es par";                    
                break;
            case 1:
            case 3:
            case 5:
            case 7:
            case 9:
                resultado +=  num + " es impar";                         
                break;
            default:
                 resultado += " no hubo coincidencia "; 
                break;
        }
        
        System.out.println(resultado);
        }else{
           System.out.println("la calificación no es valida");     
        }
        
        
        
        // se evaluará las calificaciones de los estudiantes
        // 10 ---> es nota excelente
        // 9  ---> es nota sobresaliente
        // 8 y 7 ---> es nota buena
        // 6 y 5 ---> es nota aceptable
        // menor a 5 es baja
        
        
        
    

   
        
    }
      
    
}
