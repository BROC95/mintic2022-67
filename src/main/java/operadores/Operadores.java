
package operadores;


public class Operadores {
    
    public static void main(String[] args) {
                
        float manzanas = 3.0f;
        float personas = 2.0f;
        
        float division = manzanas/personas;
        
        division = division + 2; // division += 2;
        
        float result = --division; // realiza el decrecremento y el resultado se lo asigna a la variable result
        
        int suma = (int) (1 + result); // 3
        System.out.println("suma " + suma++);  // despues de ejecutar esta linea es que incremente el valo de la variable suma por eso imprimiria 3
        
        System.out.println("suma " + suma); // acá ya imprimiria 4 porque ya tiene el incremento    
        
        System.out.println("division " + division);
         
        System.out.println("result " + result);
        
        
    }
    
}
