/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package arrays;
import java.util.Arrays;

/**
 *
 * @author aristides
 */
public class ArraysParte2 {
    
    public static void main(String[] args) {
                
         int[] vect1 = new int[]{5, 8, 4};   // crear un vector e inicializar
         
         int[] vect2 = new int[2]; // crear un vector si inicializar 
         vect2[0] = 1;
         vect2[1] = 1;
         
         int[] vect3; // crear un referencia a un vector de enteros
         
         //------------ recorrer un vector y encontrar el número menor
         
         int elMenor = vect1[0];
         
          for(int i = 1; i < vect1.length; i++ ){   
              
             if (elMenor > vect1[i]){
                 elMenor = vect1[i];
             }
          }     
 
         System.out.println(" el menor "  + elMenor);
         
         
         //-----------------
         
         int[] array1 = new int[]{2, 4, 6, 8, 10};

       int[] array2 = Arrays.copyOf(array1, array1.length);
       
       array1[0] =5;
       
       
        System.out.println("la longitud es = " + array1[0]);  // 5
        
        System.out.println("la longitud es = " + array2[0]);   // 2
         
                 
                 
       //matrices
      
       // solo creando la matriz
      
       /*
                     0  1    
            0 -->    1, 2
            1 -->    3, 4
       */
      int[][] matriz2 = new int[][]{ {1, 2}, { 3, 4} };
      
      matriz2[0][1] = 8;
      
      //------------------------------------
      
      
      int[][] matriz3 = new int[2][]; // crear una matriz, definiendo el # de filas
      //int[][] matriz4 = new int[][2]; // forma errada de crear una matriz
      
  
          
       int[] vect = new int[]{1,2,3};          
            
      matriz3[0] = vect;
      matriz3[1] = new int[2];   
     
      matriz3[1][0] = 4;      
      matriz3[1][1] = 5;    
            
       System.out.println(" matriz3[1][1]] =  " + matriz3[1][1] );
       
        /*
                     0  1 2  
            0 -->    1, 2 3   --> tamaño 3
            1 -->    4, 5     --> tamaño 2
       */
       
       for(int i = 0; i < matriz3.length; i++){
           for(int j = 0; j < matriz3[i].length; j++){
               
               int valorSuma = matriz3[i][j] + 2;
               
               System.out.print(valorSuma +  " ");
           }
           System.out.println();
       }
       
       
       //---------------------------
       
       
        char[][] matriz4 = new char[][]{ {'X', 'O'}, { 'O', 'X'} };
       
        for(int i = 0; i < matriz4.length; i++){
            for(int j = 0; j < matriz4[i].length; j++){


                System.out.print(matriz4[i][j] +  " ");
            }
            System.out.println();
        }
    }
    
}
